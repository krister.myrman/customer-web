export async function create(url, obj) {
    const res = await fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
    });
    const result = await res.json();
    return result;
}

export async function read(url, id = "") {
    const response = await fetch(url + id, {
        method: 'GET',
        credentials: 'include'
    });
    let data = await response.json();
    return data;
}

export async function update(url, obj) {
    const res = await fetch(url + obj.id, {
        method: 'PUT',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
    });
    const result = await res.json();
    return result;
}

export async function remove(url, id) {
    const res = await fetch(url + id, {
        method: 'DELETE',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const result = await res.json();
    return result;
}