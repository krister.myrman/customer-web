import Booking from "./routes/Booking.svelte";
import History from "./routes/History.svelte";
import UserDetails from "./routes/UserDetails.svelte";
import NotFound from "./routes/NotFound.svelte";
import ConfirmBooking from "./routes/ConfirmBooking.svelte";
import OrderDetails from "./routes/OrderDetails.svelte";
import Landing from "./routes/Landing.svelte";

export default {
    '/': Landing,
    '/details/:id': OrderDetails,
    '/order': Booking,
    '/history': History,
    '/userdetails': UserDetails,
    '/confirmation/:id/:name/:size': ConfirmBooking,
    '*': NotFound
}