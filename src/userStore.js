import {writable} from "svelte/store";
import {read} from "./crud";
import {loggedIn} from "./stores";

export const currentUser = writable({});
const API_URL = "http://localhost:8081/api/v1/customers/";

export async function logInUser(username, password) {
    let authUrl = 'http://localhost:8081/api/v1/customers/1';

    const response = await fetch(authUrl, {method: 'GET',
        credentials: 'include',
        headers: {'Authorization': 'Basic ' + btoa(username+':'+password)}});
    if (response.status == 200) {
        let user = await response.json();
        currentUser.set(user);
        loggedIn.set(true);

    }
    else {
        alert('Wrong Credentials!');
    }
    console.log(response);
}
// export async function logIn() {
// }
export async function getUser(id) {
    currentUser.set(await read(API_URL, id));
}

