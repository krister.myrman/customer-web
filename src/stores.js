//TODO: Fix checking dates properly, by splitting strings

import {writable, derived, get} from 'svelte/store';

export const fromDate = writable('');
export const toDate = writable('');
// export const cars = writable([]);
export const loggedIn = writable(false);
export const userId = writable(1)

// A derived store in Svelte updates when any of the stores it's based on updates, so when the user picks a new from
// date the callback that filters the list runs again and returns a new list that reactively updates the view.
// export const filteredCars = derived(
//     [cars, fromDate, toDate],
//     ([$cars, $fromDate, $toDate]) => {
//         const fromDateFormatted = getMyTime($fromDate);
//         const toDateFormatted = getMyTime($toDate);
//
//         return $cars.filter((car) => {
//             return car.orders.every(order => checkOrder(
//                 fromDateFormatted,
//                 toDateFormatted,
//                 getMyTime(order.startTime),
//                 getMyTime(order.endTime)
//             ));
//         });
//     })

// Checks if a single order is between the booking dates, returns true
function checkOrder(bookingStartDate, bookingReturnDate, orderStartDate, orderReturnDate) {
    if (bookingStartDate > orderReturnDate || bookingReturnDate < orderStartDate) {
        return true;
    }
    return false;

    return
}

function getMyTime(date) {
    return new Date(date).getTime();
}

export function getUtcFromDate() {
    return new Date(get(fromDate)).toISOString().substring(0,19);
}

export function getUtcToDate() {
    return new Date(get(toDate)).toISOString().substring(0,19);
}