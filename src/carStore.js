import {create, read, update, remove} from "./crud";
import {get, writable} from "svelte/store";
import {fromDate, getUtcFromDate, getUtcToDate} from "./stores";

export const cars = writable([]);
const API_URL = "http://localhost:8081/api/v1/cars/";

export async function getSingleCar(id) {
    return await read(API_URL, id);
}

export async function deleteCar(id) {
    await remove(API_URL, id);
    await getCars();
}

export async function createCar(car) {
    await create(API_URL, car);
    await getCars();
}

export async function updateCar(car) {
    await update(API_URL, car);
    await getCars();
}

export async function getCars() {
    console.log(get(fromDate));
    cars.set(await read("http://localhost:8081/api/v1/cars2" + "?fromDate=" +  getUtcFromDate() + "&toDate=" + getUtcToDate()));
}